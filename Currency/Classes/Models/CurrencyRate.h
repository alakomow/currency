//
//  CurrencyRate.h
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>

@interface CurrencyRate : NSObject
@property (nonatomic, retain, readonly) NSString *baseCurrency;
@property (nonatomic, retain, readonly) NSString *referenceCurrency;
@property (nonatomic, retain, readonly) NSNumber *rate;
@property (nonatomic, retain, readonly) NSDate *lastUpdate;
@property (nonatomic, assign, readonly) NSInteger percentChange;

- (instancetype)initWithBaseCurrency:(NSString *)baseCurrency
                   referenceCurrency:(NSString *)referenceCurrency
                                rate:(NSNumber *)rate
                          lastUpdate:(NSDate *)lastUpdate
                       percentChange:(NSInteger)percentChange;
@end
