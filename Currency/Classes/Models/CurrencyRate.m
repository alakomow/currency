//
//  CurrencyRate.m
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "CurrencyRate.h"

@implementation CurrencyRate
- (instancetype)initWithBaseCurrency:(NSString *)baseCurrency
                   referenceCurrency:(NSString *)referenceCurrency
                                rate:(NSNumber *)rate
                          lastUpdate:(NSDate *)lastUpdate
                       percentChange:(NSInteger)percentChange {
    if (self = [super init]) {
        _baseCurrency = baseCurrency;
        _referenceCurrency = referenceCurrency;
        _rate = rate;
        _lastUpdate = lastUpdate;
        _percentChange = percentChange;
    }
    return self;
}
@end
