//
//  APIOperation.m
//  weather
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "APIOperation.h"

#define kAPIUrl @"https://api.fixer.io/"
#define kRequestTimeOutInterval 60.f

@interface APIOperation(){
    APIOperationCompletionBlock _completionBlock;
    NSURLRequest *_request;
    NSURLSessionDataTask *_dataTask;
}

@end

@implementation APIOperation

- (instancetype)initWithCompletionBlock:(APIOperationCompletionBlock)block {
    if (self = [super init]) {
        _completionBlock = [block copy];
    }
    return self;
}

- (instancetype)initGetAllCurrencyForBaseCurrency:(NSString *)baseCurrency completion:(APIOperationCompletionBlock)block{
    if (self = [self initWithCompletionBlock:block]) {
        _request = [NSURLRequest requestWithURL:[self urlForBaseCurrency:baseCurrency]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initGetRateCurrencyForBase:(NSString *)baseCurrency referenceCurrency:(NSString *)referenceCurrency completionBlock:(APIOperationCompletionBlock)block {
    if (self = [self initWithCompletionBlock:block]) {
        _request = [NSURLRequest requestWithURL:[self urlRateCurrencyForBaseCurrency:baseCurrency
                                                                   referenceCurrency:referenceCurrency]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initGetRateForBaseCurrency:(NSString *)baseCurrency onDate:(NSString *)date completionBlock:(APIOperationCompletionBlock)block {
    if (self = [self initWithCompletionBlock:block]) {
        _request = [NSURLRequest requestWithURL:[self urlRateForBaseCurrency:baseCurrency onDate:date]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (void)start {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    _dataTask = [session dataTaskWithRequest:_request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {


        BOOL isCanceled = NO;
        if (error) {
            if ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorCancelled) {
                isCanceled = YES;
            }
            if (_completionBlock) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _completionBlock(nil,isCanceled,error);
                });
            }
            return;
        }


        NSDictionary *responceData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        if (_completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _completionBlock(responceData, isCanceled, error);
            });
        }
    }];
    [_dataTask resume];
}

- (void)stop {
    [_dataTask cancel];
}

#pragma mark - Api Urls

- (NSURL *)urlForBaseCurrency:(NSString *)baseCurrency {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"latest?base=%@",baseCurrency];
    return [NSURL URLWithString:stringURL];
}
- (NSURL *)urlRateCurrencyForBaseCurrency:(NSString *)base referenceCurrency:(NSString *)reference {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"latest?base=%@&symbols=%@",base,reference];
    return [NSURL URLWithString:stringURL];
}

- (NSURL *)urlRateForBaseCurrency:(NSString *)base onDate:(NSString *)date {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"%@?base=%@",date,base];
    return [NSURL URLWithString:stringURL];
}
@end
