//
//  APIOperation.h
//  weather
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^APIOperationCompletionBlock)(NSDictionary *responce, BOOL didCanceled, NSError *error);

@interface APIOperation : NSObject
- (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) new __attribute__((unavailable("Must use other init methods.")));

- (instancetype)initGetAllCurrencyForBaseCurrency:(NSString *)baseCurrency completion:(APIOperationCompletionBlock)block;
- (instancetype)initGetRateCurrencyForBase:(NSString *)baseCurrency referenceCurrency:(NSString *)referenceCurrency completionBlock:(APIOperationCompletionBlock)block;
- (instancetype)initGetRateForBaseCurrency:(NSString *)baseCurrency onDate:(NSString *)date completionBlock:(APIOperationCompletionBlock)block;

- (void)start;
- (void)stop;
@end
