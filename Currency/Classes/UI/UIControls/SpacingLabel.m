//
//  SpacingLabel.m
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "SpacingLabel.h"

@implementation SpacingLabel

- (void)setCharSpacing:(CGFloat)charSpacing {
    _charSpacing = charSpacing;
    self.text = self.text;
}

- (void)setText:(NSString *)text {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = self.charSpacing;
    paragraphStyle.alignment = self.textAlignment;
    NSDictionary *attributes = @{NSParagraphStyleAttributeName: paragraphStyle};
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text.length?text:@""
                                                                         attributes:attributes];
    self.attributedText = attributedText;
}
@end
