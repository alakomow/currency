//
//  CurrencyMainViewController.m
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "CurrencyMainViewController.h"
#import "CurrencyRate.h"
#import "APIOperation.h"
#import "ChangeCurrencyViewController.h"

#define kDefaulfBaseCurrency @"USD"
#define kDefaultReferenceCurrency @"RUB"
#define kDefaultPreviosDateInterval 60*60*24*1 //1 day

@interface CurrencyMainViewController ()<ChangeCurrencyViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *currencyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyTimeUpdateLabel;

@property (nonatomic, retain) CurrencyRate *currentCurrency;
@end

@implementation CurrencyMainViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self reloadCurrencyWithBase:self.currentCurrency.baseCurrency reference:self.currentCurrency.referenceCurrency];
}

#pragma mark - setters
- (void)setCurrentCurrency:(CurrencyRate *)currentCurrency {
    _currentCurrency = currentCurrency;

    self.currencyTitleLabel.text = [[currentCurrency.baseCurrency.uppercaseString stringByAppendingString:@" → "] stringByAppendingString:currentCurrency.referenceCurrency.uppercaseString];

    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm"];
    self.currencyTimeUpdateLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Updated at %@",nil),[timeFormat stringFromDate:currentCurrency.lastUpdate]];

    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:3];
    [formatter setDecimalSeparator:@","];
    [formatter setRoundingMode: NSNumberFormatterRoundDown];
    self.currencyValueLabel.text = [formatter stringFromNumber:currentCurrency.rate];

    NSString *description = [NSString stringWithFormat:NSLocalizedString(@"Since yesterday %@ feel by %@ percents", nil), currentCurrency.baseCurrency.uppercaseString,@(currentCurrency.percentChange)];
    UIColor *descriptionColor;
    if (currentCurrency.percentChange >= 0) {
        descriptionColor = [UIColor colorWithRed:126.f/255.f green:211.f/255.f blue:33.f/255.f alpha:1];
    } else {
        descriptionColor = [UIColor redColor];
    }
    self.currencyDescriptionLabel.textColor = descriptionColor;
    self.currencyDescriptionLabel.text = self.currentCurrency?description:nil;
}

#pragma mark - private
- (void)viewError:(NSError *)error {
    [self viewErrorString:error.localizedDescription];
}

- (void)viewErrorString:(NSString *)errorString {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil)
                                                                  message:errorString
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];

    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:NULL];
}

- (void)reloadCurrencyWithBase:(NSString *)base reference:(NSString *)reference {
    NSString *baseCurrency = base.length?base:kDefaulfBaseCurrency;
    NSString *referenceCurrency = reference.length?reference:kDefaultReferenceCurrency;

    self.currentCurrency = nil;

    __weak typeof(self) weakSelf = self;
    [[[APIOperation alloc] initGetRateCurrencyForBase:baseCurrency referenceCurrency:referenceCurrency completionBlock:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            return;
        }

        if (error) {
            [self viewError:error];
            return;
        }

        if ([responce.allKeys containsObject:@"error"]) {
            [self viewErrorString:[responce objectForKey:@"error"]];
            return;
        }

        NSDictionary *rates = [responce objectForKey:@"rates"];
        if (rates.count) {
            NSString *base = [responce objectForKey:@"base"];
            NSString *reference = [rates allKeys].firstObject;
            NSNumber *rate = [rates allValues].firstObject;
            NSString *dateString = [responce objectForKey:@"date"];
            if (base.length && reference.length && rate && dateString.length) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString = [dateFormatter dateFromString:dateString];

                dateFromString = [dateFromString dateByAddingTimeInterval:-kDefaultPreviosDateInterval];
                dateString = [dateFormatter stringFromDate:dateFromString];

                if (dateFromString) {
                    [[[APIOperation alloc] initGetRateForBaseCurrency:base onDate:dateString completionBlock:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
                        if (didCanceled) {
                            return;
                        }

                        if (error) {
                            [self viewError:error];
                            return;
                        }

                        if ([responce.allKeys containsObject:@"error"]) {
                            [self viewErrorString:[responce objectForKey:@"error"]];
                            return;
                        }

                        NSInteger percentChange = 0;
                        if ([[[responce objectForKey:@"rates"] allKeys] containsObject:reference]) {
                            NSNumber *oldRate = [[responce objectForKey:@"rates"] objectForKey:reference];

                            if (oldRate.floatValue != 0.f) {
                                percentChange = (100 * (rate.floatValue/oldRate.floatValue)) - 100;
                            }
                        }
                        weakSelf.currentCurrency = [[CurrencyRate alloc] initWithBaseCurrency:base
                                                                            referenceCurrency:reference
                                                                                         rate:rate
                                                                                   lastUpdate:[NSDate new]
                                                                                percentChange:percentChange];
                    }] start];

                }else {
                    weakSelf.currentCurrency = [[CurrencyRate alloc] initWithBaseCurrency:base
                                                                        referenceCurrency:reference
                                                                                     rate:rate
                                                                               lastUpdate:[NSDate new]
                                                                            percentChange:0];
                }
            }
        }
    }] start];
}

#pragma mark - ChangeCurrencyViewControllerDelegate
- (void)changeCurrencyViewController:(ChangeCurrencyViewController *)controller didSelectBaseCurrency:(NSString *)baseCurrency andReferenceCurrency:(NSString *)referenceCurrency {
    [self reloadCurrencyWithBase:baseCurrency reference:referenceCurrency];
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"openChangeCurrencyController"]) {
        ChangeCurrencyViewController *controller = segue.destinationViewController;
        controller.currentCurrency = self.currentCurrency;
        controller.delegate = self;
    }
}


@end
