//
//  ChangeCurrencyViewController.h
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CurrencyRate;
@protocol ChangeCurrencyViewControllerDelegate;

@interface ChangeCurrencyViewController : UIViewController
@property (nonatomic, retain) CurrencyRate *currentCurrency;

@property (nonatomic, weak) id<ChangeCurrencyViewControllerDelegate> delegate;
@end

@protocol ChangeCurrencyViewControllerDelegate <NSObject>
- (void)changeCurrencyViewController:(ChangeCurrencyViewController *)controller didSelectBaseCurrency:(NSString *)baseCurrency andReferenceCurrency:(NSString *)referenceCurrency;
@end
