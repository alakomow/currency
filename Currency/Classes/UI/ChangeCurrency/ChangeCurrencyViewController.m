//
//  ChangeCurrencyViewController.m
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "ChangeCurrencyViewController.h"
#import "CurrencyRate.h"
#import "CurrencyTableViewCell.h"
#import "APIOperation.h"


@interface CurrencyDataModel : NSObject
@property (nonatomic, retain, readonly) NSString *base;
@property (nonatomic, retain, readonly) NSString *reference;
+(instancetype)initWithBase:(NSString *)base reference:(NSString *)reference;
@end

@implementation CurrencyDataModel
+ (instancetype)initWithBase:(NSString *)base reference:(NSString *)reference {
    return [[self alloc] initWithBase:base reference:reference];
}

- (instancetype)initWithBase:(NSString *)base reference:(NSString *)reference {
    if (self = [super init]) {
        _base = base;
        _reference = reference;
    }
    return self;
}
@end

@interface ChangeCurrencyViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *currencyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyDescriptionLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDataPaddingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerDataConstraint;
@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listHeigthConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (nonatomic, retain) NSArray *currencyData;
@end

@implementation ChangeCurrencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currencyData = @[[CurrencyDataModel initWithBase:self.currentCurrency.baseCurrency  reference:self.currentCurrency.referenceCurrency]
                          ];

    __weak typeof(self) weakSelf = self;
    [[[APIOperation alloc] initGetAllCurrencyForBaseCurrency:self.currentCurrency.baseCurrency completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            return;
        }

        if (error) {
            [self viewErrorString:error.localizedDescription];
            return;
        }

        if ([responce.allKeys containsObject:@"error"]) {
            [self viewErrorString:[responce objectForKey:@"error"]];
            return;
        }

        NSDictionary *rates = [responce objectForKey:@"rates"];

        NSArray *currencys = [[rates.allKeys arrayByAddingObject:[responce objectForKey:@"base"]] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        if (currencys.count) {
            NSMutableArray *data = [NSMutableArray new];
            for (NSString *currencyBase in currencys) {
                for (NSString *currencyReference in currencys) {
                    if (![currencyBase isEqualToString:currencyReference]) {
                        [data addObject:[CurrencyDataModel initWithBase:currencyBase reference:currencyReference]];
                    }
                }
            }
            weakSelf.currencyData = [NSArray arrayWithArray:data];
            [weakSelf.tableView reloadData];
        }

        
    }] start];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.listHeigthConstraint.constant = 0.f;
    [self updateCurrencyUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self openMenu];
}

#pragma mark - setters
- (void)setCurrentCurrency:(CurrencyRate *)currentCurrency {
    _currentCurrency = currentCurrency;
    [self updateCurrencyUI];
}

#pragma mark - private
- (void)updateCurrencyUI {
    self.currencyTitleLabel.text = [[self.currentCurrency.baseCurrency.uppercaseString stringByAppendingString:@" → "] stringByAppendingString:self.currentCurrency.referenceCurrency.uppercaseString];

    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:3];
    [formatter setDecimalSeparator:@","];
    [formatter setRoundingMode: NSNumberFormatterRoundDown];
    self.currencyValueLabel.text = [formatter stringFromNumber:self.currentCurrency.rate];

    NSString *description = [NSString stringWithFormat:NSLocalizedString(@"Since yesterday %@ feel by %@ percents", nil), self.currentCurrency.baseCurrency.uppercaseString,@(self.currentCurrency.percentChange)];
    UIColor *descriptionColor;
    if (self.currentCurrency.percentChange >= 0) {
        descriptionColor = [UIColor colorWithRed:126.f/255.f green:211.f/255.f blue:33.f/255.f alpha:1];
    } else {
        descriptionColor = [UIColor redColor];
    }
    self.currencyDescriptionLabel.textColor = descriptionColor;
    self.currencyDescriptionLabel.text = description;
}

- (void)openMenu {

    CGFloat dataHeight = CGRectGetMaxY(self.currencyDescriptionLabel.frame) - CGRectGetMinY(self.currencyTitleLabel.frame);
    [self.view layoutIfNeeded];
    self.topDataPaddingConstraint.constant = CGRectGetMaxY([UIApplication sharedApplication].statusBarFrame) + CGRectGetHeight(self.currencyTitleLabel.frame);
    self.listHeigthConstraint.constant = CGRectGetHeight(self.view.frame) - dataHeight - CGRectGetHeight(self.currencyTitleLabel.frame) - self.topDataPaddingConstraint.constant;
    [self.dataView removeConstraint:self.centerDataConstraint];

    [UIView animateWithDuration:0.4f animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.tableView.alpha = 0;
        self.tableView.hidden = NO;
        [UIView animateWithDuration:0.4f animations:^{
            self.tableView.alpha = 1;
        }];
    }];

}

- (void)viewErrorString:(NSString *)errorString {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil)
                                                                   message:errorString
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];

    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:NULL];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.currencyData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"currencyCell";
    CurrencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    CurrencyDataModel *object = [self.currencyData objectAtIndex:indexPath.row];
    cell.currencyText = [[object.base stringByAppendingString:@" → "] stringByAppendingString:object.reference];
    if ([object.base isEqualToString:self.currentCurrency.baseCurrency]) {
        if ([object.reference isEqualToString:self.currentCurrency.referenceCurrency]) {
            [cell setCurrent];
        }
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CurrencyDataModel *object = [self.currencyData objectAtIndex:indexPath.row];

    if ([self.delegate respondsToSelector:@selector(changeCurrencyViewController:didSelectBaseCurrency:andReferenceCurrency:)]) {
        [self.delegate changeCurrencyViewController:self didSelectBaseCurrency:object.base andReferenceCurrency:object.reference];
    }
}
@end
