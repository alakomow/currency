//
//  CurrencyTableViewCell.h
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrencyTableViewCell : UITableViewCell
@property (nonatomic, retain) NSString *currencyText;
- (void)setCurrent;
@end
