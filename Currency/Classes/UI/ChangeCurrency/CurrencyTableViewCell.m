//
//  CurrencyTableViewCell.m
//  Currency
//
//  Created by Artem on 08/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "CurrencyTableViewCell.h"

@interface CurrencyTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@end

@implementation CurrencyTableViewCell

#pragma mark - setters
- (void)setCurrencyText:(NSString *)currencyText {
    _currencyText = currencyText;
    self.dataLabel.textColor = [UIColor colorWithRed:1.f green:1.f blue:1.f alpha:0.7f];
    self.dataLabel.text = currencyText;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {

}

- (void)setCurrent {
    self.dataLabel.textColor = [UIColor colorWithRed:1.f green:1.f blue:1.f alpha:1.f];
}
@end
